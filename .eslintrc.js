module.exports = {
  root: true,
  extends: [
    'standard',
    'plugin:@typescript-eslint/recommended',
    'plugin:jest/recommended',
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
    '@vue/prettier',
    '@vue/typescript',
    //prettier must be the last to override all other eslint formatter
    'prettier',
    'prettier/standard',
    'prettier/@typescript-eslint',
    'prettier/vue'
  ],
  plugins: ['@typescript-eslint', 'vue', 'standard', 'jest', 'json', 'html', 'prettier'],
  settings: {
    'html/html-extensions': ['.html'] // don't include .vue
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    es6: true,
    browser: true,
    node: true,
    jest: true,
    'jest/globals': true
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        useTabs: false,
        singleQuote: true,
        semi: true,
        trailingComma: 'none',
        arrowParens: 'always',
        printWidth: 120,
        bracketSpacing: true,
        tabWidth: 2
      }
    ], // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
};
