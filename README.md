# vue-library-starter

Use this starter if you want to build independent vue components. The starter contains:
* vue with typescript
* vuetify
* jest and ts-jest

## Executive Summary

```bash
# lint and fix
npm run lint

# compile
npm run build
```

## How to use

1. Install dependency

    ```bash
    # install dependency
    npm i vue-library-starter --save
    ```

2. Import a certain component (e.g. `YouNameIt`) into another vue component:

    ```ts
    import YouNameIt from "vue-library-starter";

    @Component({
        components: { YouNameIt }
    })
    ```

### Maintainers
* Josef Rossa